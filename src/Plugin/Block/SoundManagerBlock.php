<?php

namespace Drupal\sm_audio_player\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Sound Manager' Block.
 *
 * @Block(
 *   id = "sound_manager_player_block",
 *   admin_label = @Translation("Sound Manager block"),
 *   category = @Translation("Sound Manager"),
 * )
 */
class SoundManagerBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'audio_sound_manager_player',
    ];
  }

}
