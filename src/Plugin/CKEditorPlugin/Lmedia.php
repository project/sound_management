<?php

namespace Drupal\sm_audio_player\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;
use Drupal\media_library\MediaLibraryState;
use Drupal\media_library\MediaLibraryUiBuilder;
use Drupal\Core\Url;

/**
 * Defines the "Lmdedia" plugin.
 *
 * @CKEditorPlugin(
 *   id = "lmedia",
 *   label = @Translation("Lmdedia upload"),
 *   module = "ckeditor"
 * )
 */
class Lmedia extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('extension.list.module'),
      $container->get('entity_type.manager')
    );
  }

  /**
   *
   */
  public function getFile() {
    return drupal_get_path('module', 'sm_audio_player') . '/js/plugins/lmedia/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [
      'drupalmedia',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'editor/drupal.editor.dialog',
    ];
  }

  /**
   *
   */
  public function getSelectionResponse(MediaLibraryState $state, array $selected_ids) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {

    $entity_type_manager = \Drupal::entityTypeManager();
    $media_storage = $entity_type_manager->getStorage('media_type');

    $media_type_ids = $media_storage->getQuery()->execute();

    if ($editor->hasAssociatedFilterFormat()) {
      if ($media_embed_filter = $editor->getFilterFormat()->filters()->get('media_embed')) {
        // Optionally limit the allowed media types based on the MediaEmbed
        // setting. If the setting is empty, do not limit the options.
        if (!empty($media_embed_filter->settings['allowed_media_types'])) {
          $media_type_ids = array_intersect_key($media_type_ids, $media_embed_filter->settings['allowed_media_types']);
        }
      }
    }

    if (in_array('image', $media_type_ids, TRUE)) {
    
      array_unshift($media_type_ids, 'image');
      $media_type_ids = array_unique($media_type_ids);
    }

    $state = MediaLibraryState::create(
      'media_library.opener.editor',
      $media_type_ids,
      reset($media_type_ids),
      1,
      ['filter_format_id' => $editor->getFilterFormat()->id()]
    );
    return [
      'DrupalMediaLibrary_url_custom' => Url::fromRoute('media_library.ui')
        ->setOption('query', $state->all())
        ->toString(TRUE)
        ->getGeneratedUrl(),
      'DrupalMediaLibrary_dialogOptions_custom' => MediaLibraryUiBuilder::dialogOptions(),
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {

    $path = drupal_get_path('module', 'sm_audio_player') . '/js/plugins/lmedia';
    return [
      'lmediabutton' => [
        'label' => $this->t('Lmdeia'),
        'image' => $path . '/file.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\editor\Form\EditorFileDialog
   * @see sm_audio_player_settings_form()
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
     return [];
  }

}
