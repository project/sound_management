/**
 * @file
 * Drupal File plugin.
 *
 * @ignore
 */

(function ($, Drupal, drupalSettings, CKEDITOR) {

  "use strict";
 
  CKEDITOR.plugins.add('lmedia', {
    init: function (editor) {
      // Add the commands for file and unfile.
      editor.addCommand('lmedia', {
        allowedContent: {
          lmedia: {
            attributes: {
              '!data-entity-type': true,
              '!data-entity-uuid': true
            },
            classes: {}
          }
        },
        requiredContent: new CKEDITOR.style({
          element: 'lmedia',
          attributes: {
            'data-entity-type': '',
            'data-entity-uuid': ''
          }
        }),
        modes: {wysiwyg: 1},
        canUndo: true,
        exec: function (editor) {
          var muri;
          var sel = editor.getSelection();
          var sel1 = sel.getSelectedText();
          var drupalImageUtils = CKEDITOR.plugins.drupalimage;
          var focusedImageWidget = drupalImageUtils && drupalImageUtils.getFocusedWidget(editor);
          var fileElement = getSelectedFile(editor);

          // Set existing values based on selected element.
          
          // Prepare a save callback to be used upon saving the dialog.
          var saveCallback = function (returnValues) {
            // If an image widget is focused, we're not editing an independent
            // link, but we're wrapping an image widget in a link.
            if (focusedImageWidget) {
              focusedImageWidget.setData('link', CKEDITOR.tools.extend(returnValues.attributes, focusedImageWidget.data.link));
              editor.fire('saveSnapshot');
              return;
            }
            
           var uuid = returnValues.attributes['data-entity-uuid'];
            
             // console.log(attributes['data-entity-uuid']);
             jQuery.ajax({
                      url: Drupal.url('/api/json/'+uuid),
                      type: "GET",
                       dataType: "json",
                      beforeSend: function(x) {
                        if (x && x.overrideMimeType) {
                          x.overrideMimeType("application/json;charset=UTF-8");
                        }
                      },
                      success: function(result) {
      
 
            editor.fire('saveSnapshot');
           var smhtml = '<span class ="outoflist"><a href="'+ result.data.uri +'"><span class="label">'+ sel1 +'</span></a></span>';
           var custom_quote = editor.document.createElement('span');
            custom_quote.setHtml(smhtml);
            editor.insertElement(custom_quote);  
            editor.fire('saveSnapshot');
                      }
            });
 
          }; 
        
         Drupal.ckeditor.openDialog(editor, editor.config.DrupalMediaLibrary_url_custom, {}, saveCallback, editor.config.DrupalMediaLibrary_dialogOptions_custom);


          
        }
      });
 
      if (editor.ui.addButton) {
        editor.ui.addButton('lmediabutton', {
          label: Drupal.t('Sound manager player'),
          command: 'lmedia',
          icon: this.path + '/file.png'
        });
      }

      // If the "menu" plugin is loaded, register the menu items.
      if (editor.addMenuItems) {
        editor.addMenuItems({
          file: {
            label: Drupal.t('Edit File'),
            command: 'lmedia',
            group: 'link',
            order: 1
          }
        });
      }

      // If the "contextmenu" plugin is loaded, register the listeners.
      if (editor.contextMenu) {
        editor.contextMenu.addListener(function (element, selection) {
          if (!element || element.isReadOnly()) {
            return null;
          }
          var anchor = getSelectedFile(editor);
          if (!anchor) {
            return null;
          }

          var menu = {};
          if (anchor.getAttribute('href') && anchor.getChildCount()) {
            menu = {file: CKEDITOR.TRISTATE_OFF};
          }
          return menu;
        });
      }
    }
  });

  function getSelectedFile(editor) {
    var selection = editor.getSelection();
    var selectedElement = selection.getSelectedElement();
    
    
    if (selectedElement && selectedElement.is('a')) {
      return selectedElement;
    }

    var range = selection.getRanges(true)[0];

    if (range) {
      range.shrink(CKEDITOR.SHRINK_TEXT);
      return editor.elementPath(range.getCommonAncestor()).contains('a', 1);
    }
    return null;
  }
  

})(jQuery, Drupal, drupalSettings, CKEDITOR);
