Sound Management
================

The Sound Management module aims to provide a framework for audio streaming sites.

## Installation

- `composer require drupal/sound_management`
- Download the SoundManager2 library from https://github.com/scottschiller/SoundManager2 to yourdomain.com/libraries/SoundManager2-master

Assuming you are using the standard Drupal folder structure, these steps should add the SoundManager2 library correctly:

```
mkdir -p web/libraries
cd web/libraries
git clone https://github.com/scottschiller/SoundManager2 SoundManager2-master
```

## Credits
Developed by https://www.drupal.org/u/raghvendrag from https://www.drupal.org/cyber-infrastructure-cisin
Sponsored by https://tambourhinoceros.net, an independent record label and music publisher
Maintained by Tambourhinoceros founder https://drupal.org/u/kristofferrom
